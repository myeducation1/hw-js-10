let display = document.getElementById("display");
let calculText = "";
let resultText = "";
let displayText = "";
let memory = "";
let spanMemoryPlus = document.getElementById("mm");

let one = document.getElementById("one").addEventListener("click", () => {
    innerDisplay(1);
	return one = 1
});
let two = document.getElementById("two").addEventListener("click", () => {
    innerDisplay(2);
	return two = 2
});
let thre = document.getElementById("thre").addEventListener("click", () => {
    innerDisplay(3);
	return thre = 3
});
let plus = document.getElementById("plus").addEventListener("click", () => {
    arifmetikOptom("+");
	return plus = "+"
});
let zero = document.getElementById("zero").addEventListener("click", () => {
    innerDisplay(0);
	return zero = 0
});
let float = document.getElementById("float").addEventListener("click", () => {
    innerDisplay(".");
	return float = "."
});
let fore = document.getElementById("fore").addEventListener("click", () => {
    innerDisplay(4);
	return fore = 4
});
let five = document.getElementById("five").addEventListener("click", () => {
    innerDisplay(5);
	return five = 5
});
let six = document.getElementById("six").addEventListener("click", () => {
    innerDisplay(6);
	return six = 6
});
let minus = document.getElementById("minus").addEventListener("click", () => {
    arifmetikOptom("-");
	return minus = "-"
});
let seven = document.getElementById("seven").addEventListener("click", () => {
    innerDisplay("7");
	return seven = 7
});
let eit = document.getElementById("eit").addEventListener("click", () => {
    innerDisplay(8);
	return eit = 8
});
let nine = document.getElementById("nine").addEventListener("click", () => {
    innerDisplay(9);
	return nine = 9
});
let mnozh = document.getElementById("mnozh").addEventListener("click", () => {
    arifmetikOptom("*");
	return mnozh = "*"
});
let dilesh = document.getElementById("dilesh").addEventListener("click", () => {
    arifmetikOptom("/");
	return dilesh = "/"
});
let zatirka = document.getElementById("zatirka").addEventListener("click", () => {
	cancelAll();
	cancelResult();
});
let rezult = document.getElementById("rezult").addEventListener("click", () => {resultFun()});
let memoryPlus = document.getElementById("m+").addEventListener("click", () => {
	memoryAdd();
	return spanMemoryPlus.innerHtml = "m";
});
let memoryMinus = document.getElementById("m-").addEventListener("click", () => {
	memoryAdd();
	return spanMemoryPlus.innerHtml = "m";
});
let mrc = document.getElementById("mrc").addEventListener("click", () => {mrcShow()});


let innerDisplay = function (a) {
	calculText = calculText.concat(a);
	displayText = displayText.concat(a);
	return display.textContent = displayText;
}

let cancelAll = function(){
	calculText = "";
	displayText = "";
	return display.textContent = displayText;
}

let resultFun = function(){
	resultText = eval(calculText);
	calculText = "";
    return display.textContent = resultText;
}

let cancelResult = function(){
	resultText = "";
	displayText = "";
	return display.textContent = displayText;
}

let arifmetikOptom = function(a){
	displayText = "";
	calculText = calculText.concat(a);
	return display.textContent = displayText;
}

let memoryAdd = function(){
	memory = display.textContent;
	display.textContent = "";
	return spanMemoryPlus.textContent = "m";
}

let mrcShow = function(){
    if (display.textContent !== memory) {
    	return display.textContent = memory;
    }else{
    	display.textContent = "";
    	memory = "";
    	return spanMemoryPlus.textContent = "";
    }
}